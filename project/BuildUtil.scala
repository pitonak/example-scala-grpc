object BuildUtil {
  implicit class ApplyOps[A](private val value: A) extends AnyVal {
    def apply[B](f: A => B): B = f(value)
  }
}
