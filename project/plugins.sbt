addSbtPlugin("io.spray" % "sbt-revolver" % "0.9.1")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.7.5")

addSbtPlugin("com.lightbend.akka.grpc" % "sbt-akka-grpc" % "1.0.2")
addSbtPlugin("com.thesamet" % "sbt-protoc" % "1.0.0-RC2")
addSbtPlugin("io.higherkindness" %% "sbt-mu-srcgen" % "0.23.0")

libraryDependencies += "com.thesamet.scalapb.zio-grpc" %% "zio-grpc-codegen" % "0.4.0"

// https://github.com/higherkindness/mu-scala/issues/923
dependencyOverrides += "com.julianpeeters" %% "avrohugger-core" % "1.0.0-RC21"
