package example.grpcakka

import java.security.{KeyStore, SecureRandom}
import java.security.cert.{Certificate, CertificateFactory}

import akka.actor.ActorSystem
import akka.grpc.GrpcClientSettings
import akka.http.scaladsl._
import akka.http.scaladsl.server.Route
import akka.pki.pem.{DERPrivateKeyLoader, PEMDecoder}
import akka.util.Timeout
import javax.net.ssl.{KeyManagerFactory, SSLContext}
import org.slf4j.LoggerFactory

import scala.concurrent._
import scala.concurrent.duration._
import scala.io.Source
import scala.util.chaining._

object HelloServer {
  private val logger = LoggerFactory.getLogger(getClass)

  private val host = "0.0.0.0"
  private val httpPort = 8080
  private val grpcPort = 9000

  def main(args: Array[String]): Unit = {
    implicit val system: ActorSystem = ActorSystem("HelloServer")
    implicit val executionContext: ExecutionContext = system.dispatcher
    implicit val timeout: Timeout = Timeout(5.seconds)

    val helloClient = HelloServiceClient(GrpcClientSettings.fromConfig("grpcakka.HelloService"))
    val helloRoutes = new HelloRoutes(helloClient)

    val httpServer = Http(system)
      .newServerAt(host, httpPort)
      .bind(Route.seal(helloRoutes.routes))
      .await
      .tap { _ =>
        logger.info("HTTP Server Started")
      }
      .addToCoordinatedShutdown(10.seconds)

    val grpcServer = Http(system)
      .newServerAt(host, grpcPort)
      .enableHttps(httpContext())
      .bind(HelloServiceHandler(new HelloServiceImpl))
      .await
      .tap { _ =>
        logger.info("gRPC Server Started")
      }
      .addToCoordinatedShutdown(10.seconds)

    httpServer.whenTerminated.await
    grpcServer.whenTerminated.await
  }

  private def httpContext(): HttpsConnectionContext = {
    val privateKey = DERPrivateKeyLoader.load(PEMDecoder.decode(readPrivateKeyPem()))
    val fact = CertificateFactory.getInstance("X.509")
    val cer = fact.generateCertificate(getClass.getResourceAsStream("/certs/server1.pem"))
    val ks = KeyStore.getInstance("PKCS12")
    ks.load(null)
    ks.setKeyEntry("private", privateKey, new Array[Char](0), Array[Certificate](cer))
    val keyManagerFactory = KeyManagerFactory.getInstance("SunX509")
    keyManagerFactory.init(ks, null)
    val context = SSLContext.getInstance("TLS")
    context.init(keyManagerFactory.getKeyManagers, null, new SecureRandom)
    ConnectionContext.httpsServer(context)
  }

  private def readPrivateKeyPem(): String =
    Source.fromResource("certs/server1.key").mkString

  implicit class FutureOps[A](private val future: Future[A]) extends AnyVal {
    def await: A = Await.result(future, Duration.Inf)
  }
}
