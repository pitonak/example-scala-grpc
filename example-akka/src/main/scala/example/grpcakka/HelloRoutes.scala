package example.grpcakka

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.util.Timeout
import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport._
import io.circe.generic.JsonCodec
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext

@JsonCodec
case class HelloHttpResponse(message: String)

class HelloRoutes(helloService: HelloService)(implicit actorSystem: ActorSystem, timeout: Timeout, ec: ExecutionContext) {
  private val logger = LoggerFactory.getLogger(getClass)

  val routes: Route = path("hello") {
    parameter("name" ? "world") { (name: String) =>
      logger.info(name)
      complete {
        helloService.sayHello(HelloRequest(name)).map { response =>
          HelloHttpResponse(response.message)
        }
      }
    }
  }
}
