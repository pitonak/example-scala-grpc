package example.grpcakka
import org.slf4j.LoggerFactory

import scala.concurrent.Future

class HelloServiceImpl extends HelloService {
  private val logger = LoggerFactory.getLogger(getClass)

  def sayHello(request: HelloRequest): Future[HelloResponse] = {
    logger.info(request.toString)
    Future.successful(HelloResponse(s"Hello ${request.name}"))
  }
}
