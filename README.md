# example-scala-grpc

This repository contains a comparison of several gRPC libraries in Scala


## Motivation

gRPC implementations impose restrictions with the way they're tied to protobuf.
That means a lot of coding happens outside the host language (Scala) and instead in protobuf files,
making you do your data modelling in protobuf instead of Scala with all its restrictions.
Protobuf data models should probably be kept exclusive to gRPC API anyway and shouldn't leak into application.
That unfortunately means a lot of translation between data classes is required.
Same can be said about services, which are also defined in protobuf. Scala code is generated from these
which means you once again are forced to maintain protobuf code and not Scala code.


*NOTE* Protocol and client definition should always be in a separate module,
but it is kept in the same as server here for simplicity.


## PROTOBUF

### PROS:

**NONE IMHO**

### CONS:

* Generated source data classes
    * Hard to define companion members
    * Hard to define implicit instances
    * I strongly prefer (Scala) code as source of truth.
      Scala is our domain where we can use and enforce good practices.


## akka-grpc

* only https
* only async (Future)
* .proto file name is not used as package


## scalapb

* Sync and async client, async only server


## zio-grpc

* it's ZIO, a little too complex, but everything is covered, from resource management to streams


## mu-scala

* package nonsense
* macro expansion without IJ support
* tagless final (cats-effect)
* weird naming convention
* avro support
* nicer generated code, but still generated
