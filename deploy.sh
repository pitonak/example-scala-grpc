#!/usr/bin/env zsh

echo "[setup]"
projectName=example-scala-grpc

#tag=$(git log -1 --pretty=%H)
tag="0.1.0-SNAPSHOT"

host=gcr.io
projectId=chili-piper-sandbox
imageName="$host/$projectId/$projectName:$tag"

echo "imageName=$imageName"

echo "[sbt]"
sbt clean stage docker:publish || exit 1

echo "[kubectl]"
kubectl apply -f k8s.yaml
