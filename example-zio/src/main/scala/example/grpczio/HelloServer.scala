package example.grpczio

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.http.scaladsl._
import akka.http.scaladsl.server.Route
import akka.util.Timeout
import example.grpczio.ZioGrpczio.HelloServiceClient
import io.grpc.ManagedChannelBuilder
import org.slf4j.LoggerFactory
import scalapb.zio_grpc._

import scala.concurrent._
import scala.concurrent.duration._
import scala.util.chaining._
import zio._

object HelloServer extends App {
  private val logger = LoggerFactory.getLogger(getClass)

  private val host = "0.0.0.0"
  private val httpPort = 8080
  private val grpcPort = 9000

  def run(args: List[String]): URIO[ZEnv, ExitCode] = for {
    _ <- httpServer().foldCause(_ => ExitCode.failure, _ => ExitCode.success)
    code <- HelloGrpcServer.run(args)
  } yield code

  def httpServer(): RIO[ZEnv, Unit] = {
    implicit val system: ActorSystem = ActorSystem("HelloServer")
    implicit val executionContext: ExecutionContext = system.dispatcher
    implicit val timeout: Timeout = Timeout(5.seconds)

    val clientLayer = HelloServiceClient.live(ZManagedChannel(
      ManagedChannelBuilder.forAddress(host, grpcPort).tap(_.usePlaintext())
    ))
    val helloRoutes = new HelloRoutes(clientLayer)

    for {
      binding <- ZIO.fromFuture { implicit ec =>
        Http(system)
          .newServerAt(host, httpPort)
          .bind(Route.seal(helloRoutes.routes))
      }
      _ <- URIO(logger.info("HTTP Server Started"))
      _ <- ZIO(binding.addToCoordinatedShutdown(10.seconds))
    } yield {}
  }

  object HelloGrpcServer extends ServerMain {
    override def port: Int = grpcPort

    def services: ServiceList[ZEnv] =
      ServiceList.add(new HelloServiceImpl)
  }
}
