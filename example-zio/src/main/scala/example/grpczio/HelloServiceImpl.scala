package example.grpczio

import io.grpc.Status
import org.slf4j.LoggerFactory
import zio._

class HelloServiceImpl extends ZioGrpczio.ZHelloService[ZEnv, Any] {
  private val logger = LoggerFactory.getLogger(getClass)

  def sayHello(request: HelloRequest): ZIO[ZEnv, Status, HelloResponse] =
    for {
      _ <- UIO(logger.info(request.toString))
      response <- ZIO(HelloResponse(s"Hello: ${request.name}")).mapError(toStatus)
    } yield response

  private def toStatus(t: Throwable): Status =
    Status.INTERNAL.withDescription(t.getMessage).withCause(t)
}
