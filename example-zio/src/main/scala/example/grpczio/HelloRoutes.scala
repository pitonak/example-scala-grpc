package example.grpczio

import akka.http.scaladsl.marshalling._
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport._
import example.grpczio.ZioGrpczio.HelloServiceClient
import io.circe.generic.JsonCodec
import org.slf4j.LoggerFactory
import zio._

@JsonCodec
case class HelloHttpResponse(message: String)

class HelloRoutes(helloClientLayer: Layer[Throwable, HelloServiceClient]) {
  private val logger = LoggerFactory.getLogger(getClass)

  def hello(name: String): RIO[HelloServiceClient, HelloHttpResponse] =
    HelloServiceClient.sayHello(HelloRequest(name))
      .mapError(_.getCause)
      .map(response => HelloHttpResponse(response.message))

  val routes: Route = path("hello") {
    parameter("name" ? "world") { (name: String) =>
      logger.info(name)
      complete {
        hello(name).provideLayer(helloClientLayer)
      }
    }
  }

  implicit def ioToMarshallable[E, A](
    implicit
    me: Marshaller[E, HttpResponse],
    ma: Marshaller[A, HttpResponse],
  ): Marshaller[IO[E, A], HttpResponse] = {
    Marshaller { implicit ec =>
      effect =>
        val promise = scala.concurrent.Promise[List[Marshalling[HttpResponse]]]()
        val marshalledEffect: IO[Throwable, List[Marshalling[HttpResponse]]] =
          effect.foldM(
            e => IO.fromFuture(_ => me(e)),
            a => IO.fromFuture(_ => ma(a))
          )
        HelloServer.unsafeRunAsync(marshalledEffect) { done =>
          done.fold(
            failed => promise.failure(failed.squash),
            success => promise.success(success),
          )
        }
        promise.future
    }
  }
}
