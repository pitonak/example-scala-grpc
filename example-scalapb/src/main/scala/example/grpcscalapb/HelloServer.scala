package example.grpcscalapb

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.http.scaladsl._
import akka.http.scaladsl.server.Route
import akka.util.Timeout
import io.grpc.ManagedChannelBuilder
import org.slf4j.LoggerFactory

import scala.concurrent._
import scala.concurrent.duration._
import scala.util.chaining._

object HelloServer {
  private val logger = LoggerFactory.getLogger(getClass)

  private val host = "0.0.0.0"
  private val httpPort = 8080
  private val grpcPort = 9000

  def main(args: Array[String]): Unit = {
    implicit val system: ActorSystem = ActorSystem("HelloServer")
    implicit val executionContext: ExecutionContext = system.dispatcher
    implicit val timeout: Timeout = Timeout(5.seconds)

    val helloChannel = ManagedChannelBuilder.forAddress(host, grpcPort)
      .tap { builder =>
        builder.usePlaintext()
      }
      .build()
    val helloClient = HelloServiceGrpc.stub(helloChannel)
    val helloRoutes = new HelloRoutes(helloClient)

    val httpServer = Http(system)
      .newServerAt(host, httpPort)
      .bind(Route.seal(helloRoutes.routes))
      .await
      .tap { _ =>
        logger.info("HTTP Server Started")
      }
      .addToCoordinatedShutdown(10.seconds)

    val grpcServer = io.grpc.ServerBuilder.forPort(grpcPort)
      .tap { builder =>
        builder.addService(
          HelloServiceGrpc.bindService(new HelloServiceImpl, executionContext),
        )
      }
      .build()
      .start()
      .tap { _ =>
        logger.info("gRPC Server Started")
      }

    sys.addShutdownHook {
      logger.info("Shutting down")
      grpcServer.shutdown()
      grpcServer.awaitTermination(10, TimeUnit.SECONDS)
      logger.info("Server shut down")
      helloChannel.shutdown()
      helloChannel.awaitTermination(10, TimeUnit.SECONDS)
      logger.info("Channel shut down")
    }

    httpServer.whenTerminated.await
  }

  implicit class FutureOps[A](private val future: Future[A]) extends AnyVal {
    def await: A = Await.result(future, Duration.Inf)
  }
}
