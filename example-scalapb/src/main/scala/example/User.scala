package example

import java.util.UUID
import java.util.concurrent.TimeUnit

import example.profiles._
import example.profiles.ProfileServiceGrpc.ProfileService
import io.grpc._
import org.slf4j.LoggerFactory

import scala.concurrent._
import scala.io.StdIn

case class Token(profileId: String)

case class User(
  id: UUID,
  name: String,
  pictureUrl: Option[String],
)

trait UserService {
  def save(user: User): Future[User]
}

class UserServiceImpl extends UserService {
  private val logger = LoggerFactory.getLogger(getClass)
  def save(user: User): Future[User] = {
    logger.info(s"save($user)")
    Future.successful(user)
  }
}

trait LoginService {
  def login(token: Token): Future[User]
}

class LoginServiceImpl(
  userService: UserService,
  profileService: ProfileService,
)(implicit ec: ExecutionContext) extends LoginService {
  private val logger = LoggerFactory.getLogger(getClass)

  def login(token: Token): Future[User] = {
    logger.info(s"onLogin($token)")
    val request = ProfileRequest(token.profileId)
    for {
      response <- profileService.get(request)
      profile <- validate(request, response)
      user <- userService.save(User(UUID.randomUUID(), profile.name, profile.pictureUrl))
    } yield user
  }

  private def validate(request: ProfileRequest, response: ProfileResponse): Future[Profile] =
    response.profile match {
      case Some(profile) => Future.successful(profile)
      case None => Future.failed(new RuntimeException(s"Profile ${request.id} not found"))
    }
}

class ProfileServiceImpl extends ProfileService {
  private val logger = LoggerFactory.getLogger(getClass)

  def get(request: ProfileRequest): Future[ProfileResponse] = {
    logger.info(s"get($request)")
    val profile = Profile(
      id = "5fa3f0604d12a30c109d55a0",
      name = "Luke Skywalker",
      pictureUrl = Some("https://i1.sndcdn.com/avatars-000197746117-ccha94-t500x500.jpg"),
    )
    Future.successful(ProfileResponse(Some(profile)))
  }
}

case class ChannelConfig(
  host: String,
  port: Int,
)

object ServerApp {
  implicit val ec: ExecutionContext = ExecutionContext.global
  private val config = ChannelConfig("0.0.0.0", 8080)
  private val profileService: ProfileService = new ProfileServiceImpl
  private val profileServer: Server = ServerBuilder.forPort(config.port)
    .addService(ProfileServiceGrpc.bindService(profileService, ec))
    .build

  def main(args: Array[String]): Unit = {
    profileServer.start()

    sys.addShutdownHook {
      profileServer.shutdown()
      profileServer.awaitTermination(10, TimeUnit.SECONDS)
    }
  }
}

object ClientApp {
  implicit val ec: ExecutionContext = ExecutionContext.global
  private val config = ChannelConfig("0.0.0.0", 8080)
  private val userService: UserService = new UserServiceImpl
  private val channel: ManagedChannel = ManagedChannelBuilder.forAddress(config.host, config.port)
    .usePlaintext()
    .build
  private val profileService: ProfileService = ProfileServiceGrpc.stub(channel)
  private val loginService: LoginService = new LoginServiceImpl(userService, profileService)

  def main(args: Array[String]): Unit = {
    loginService.login(Token("test"))

    sys.addShutdownHook {
      channel.shutdown()
      channel.awaitTermination(10, TimeUnit.SECONDS)
    }
  }
}

object TestApp {
  def main(args: Array[String]): Unit = {
    ServerApp.main(args)
    ClientApp.main(args)

    StdIn.readLine("Press enter to exit...")
  }
}
