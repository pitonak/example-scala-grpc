import BuildUtil._
import higherkindness.mu.rpc.srcgen.Model._

onChangedBuildSource := ReloadOnSourceChanges

ThisBuild / name := "example-scala-grpc"
ThisBuild / scalaVersion := "2.13.3"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "example"
ThisBuild / organizationName := "example"

lazy val commonSettings: Project => Project = _
  .enablePlugins(JavaAppPackaging, DockerPlugin)
  .settings(
    scalacOptions += "-Ymacro-annotations",

    dockerBaseImage := "openjdk:latest",
    dockerExposedPorts ++= Seq(8080, 9000),
    dockerRepository := Some("gcr.io/chili-piper-sandbox"),
    Universal / javaOptions ++= Seq(
      "-Dlogback.configurationFile=logback-cloud.xml",
    ),
  )
  .settings(
    libraryDependencies += "com.typesafe.akka" %% "akka-http" % "10.2.0",
    libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.6.9",
    libraryDependencies += "com.typesafe.akka" %% "akka-slf4j" % "2.6.9",
    libraryDependencies += "de.heikoseeberger" %% "akka-http-circe" % "1.34.0",
    libraryDependencies += "io.circe" %% "circe-core" % "0.13.0",
    libraryDependencies += "io.circe" %% "circe-generic" % "0.13.0",
    libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3",
    libraryDependencies += "com.google.cloud" % "google-cloud-logging" % "1.102.0",
    libraryDependencies += "com.google.cloud" % "google-cloud-logging-logback" % "0.118.2-alpha",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.0" % Test,
  )

lazy val akkaGrpc = (project in file("example-akka"))
  .apply(commonSettings)
  .enablePlugins(AkkaGrpcPlugin)
  .settings(
    name := "example-scala-grpc-akka",
    libraryDependencies += "com.typesafe.akka" %% "akka-http2-support" % "10.2.0",
    libraryDependencies += "com.typesafe.akka" %% "akka-discovery" % "2.6.9",
    libraryDependencies += "com.typesafe.akka" %% "akka-pki" % "2.6.9",
  )

lazy val scalapbGrpc = (project in file("example-scalapb"))
  .apply(commonSettings)
  .settings(
    name := "example-scala-grpc-scalapb",
    libraryDependencies += "io.grpc" % "grpc-netty" % scalapb.compiler.Version.grpcJavaVersion,
    libraryDependencies += "com.thesamet.scalapb" %% "compilerplugin" % scalapb.compiler.Version.scalapbVersion,
    libraryDependencies += "com.thesamet.scalapb" %% "scalapb-runtime-grpc" % scalapb.compiler.Version.scalapbVersion,
    Compile / PB.targets := Seq(
      scalapb.gen() -> (Compile / sourceManaged).value
    ),
  )

lazy val zioGrpc = (project in file("example-zio"))
  .apply(commonSettings)
  .settings(
    name := "example-scala-grpc-zio",
    Compile / PB.targets := Seq(
      scalapb.gen(grpc = true) -> (Compile / sourceManaged).value,
      scalapb.zio_grpc.ZioCodeGenerator -> (Compile / sourceManaged).value,
    ),
    libraryDependencies += "io.grpc" % "grpc-netty" % scalapb.compiler.Version.grpcJavaVersion,
    libraryDependencies += "com.thesamet.scalapb" %% "scalapb-runtime-grpc" % scalapb.compiler.Version.scalapbVersion,
  )

lazy val muGrpcProtocol = (project in file("example-mu/protocol"))
  .apply(commonSettings)
  .enablePlugins(SrcGenPlugin)
  .settings(
    name := "example-scala-mu-protocol",
    libraryDependencies += "io.higherkindness" %% "mu-rpc-service" % "0.23.0",
    libraryDependencies += scalaOrganization.value % "scala-compiler" % scalaVersion.value % Provided,
    muSrcGenIdlType := IdlType.Proto,
  )

lazy val muGrpc = (project in file("example-mu"))
  .apply(commonSettings)
  .dependsOn(muGrpcProtocol)
  .settings(
    name := "example-scala-mu",
    libraryDependencies += "io.higherkindness" %% "mu-rpc-server" % "0.23.0",
    libraryDependencies += "io.higherkindness" %% "mu-rpc-client-netty" % "0.23.0",
  )
