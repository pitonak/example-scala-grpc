package example.grpcmu

import cats.Applicative
import cats.implicits._
import example.grpcmu.proto._
import org.slf4j.LoggerFactory

class HelloServiceImpl[F[_]: Applicative] extends HelloService[F] {
  private val logger = LoggerFactory.getLogger(getClass)

  def SayHello(request: HelloRequest): F[HelloResponse] = {
    logger.info(request.toString)
    HelloResponse(s"Hello ${request.name}").pure[F]
  }
}
