package example.grpcmu

import akka.http.scaladsl.marshalling.{Marshaller, Marshalling}
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import cats.effect.{ContextShift, IO}
import cats.implicits._
import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport._
import example.grpcmu.proto._
import io.circe.generic.JsonCodec
import org.slf4j.LoggerFactory

@JsonCodec
case class HelloHttpResponse(message: String)

class HelloRoutes(helloService: HelloService[IO])(implicit cs: ContextShift[IO]) {
  private val logger = LoggerFactory.getLogger(getClass)

  val routes: Route = path("hello") {
    parameter("name" ? "world") { (name: String) =>
      logger.info(name)
      complete {
        helloService.SayHello(HelloRequest(name)).map { response =>
          HelloHttpResponse(response.message)
        }
      }
    }
  }


  implicit def ioToMarshallable[A](
    implicit
    ma: Marshaller[A, HttpResponse],
  ): Marshaller[IO[A], HttpResponse] = {
    Marshaller { implicit ec =>
      effect =>
        val promise = scala.concurrent.Promise[List[Marshalling[HttpResponse]]]()
        effect.flatMap(a => IO.fromFuture(IO(ma(a))))
          .unsafeRunAsync {
            case Left(error) => promise.failure(error)
            case Right(value) => promise.success(value)
          }
        promise.future
    }
  }
}
