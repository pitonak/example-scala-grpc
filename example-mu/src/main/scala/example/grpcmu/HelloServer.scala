package example.grpcmu

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.Http.ServerBinding
import akka.util.Timeout
import cats.effect.{ExitCode, IO, IOApp, Resource}
import example.grpcmu.proto.HelloService
import higherkindness.mu.rpc.server.{AddService, GrpcServer}
import higherkindness.mu.rpc.ChannelForAddress
import org.slf4j.LoggerFactory

import scala.concurrent._
import scala.concurrent.duration._

object HelloServer extends IOApp {
  private val logger = LoggerFactory.getLogger(getClass)

  private val host = "0.0.0.0"
  private val httpPort = 8080
  private val grpcPort = 9000

  def run(args: List[String]): IO[ExitCode] =
    HelloClient().flatMap(HttpServer)
      .use(_ => IO.never)
      .as(ExitCode.Success)

  def HelloClient(): Resource[IO, HelloService[IO]] = {
    val channelFor = ChannelForAddress(host, grpcPort)
    HelloService.client[IO](channelFor)
  }

  def HttpServer(helloClient: HelloService[IO]): Resource[IO, ServerBinding] = {
    val acquire = {
      for {
        binding <- IO.fromFuture {
          implicit val system: ActorSystem = ActorSystem("HelloServer")
          implicit val executionContext: ExecutionContext = system.dispatcher
          implicit val timeout: Timeout = Timeout(5.seconds)

          val helloRoutes = new HelloRoutes(helloClient)

          IO {
            Http(system)
              .newServerAt(host, httpPort)
              .bind(Route.seal(helloRoutes.routes))
          }
        }
        _ <- IO(logger.info("Http running"))
      } yield binding
    }

    val release = { (binding: ServerBinding) =>
      IO.fromFuture(IO(binding.terminate(10.seconds))).void
    }

    Resource.make(acquire)(release)
  }
}

object HelloGrpcServer extends IOApp {
  private val logger = LoggerFactory.getLogger(getClass)

  private val host = "0.0.0.0"
  private val httpPort = 8080
  private val grpcPort = 9000

  implicit val helloService: HelloService[IO] = new HelloServiceImpl[IO]

  def run(args: List[String]): IO[ExitCode] = {
    for {
      serviceDef <- HelloService.bindService[IO]
      server <- GrpcServer.default[IO](grpcPort, List(AddService(serviceDef)))
      _ <- GrpcServer.server[IO](server)
      _ <- IO(logger.info("gRPC Server Started"))
    } yield ExitCode.Success
  }
}
